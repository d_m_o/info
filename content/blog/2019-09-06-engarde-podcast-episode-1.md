---
title: 'EnGarde! The Guardian Project Podcast: Episode 1'
categories:
  - Podcast
  - News
tags:
  - podcast
  - orbot
  - orbotmini
  - matrix
  - tor
  - keanu
---

EnGarde! is our first attempt at creating a regular podcast, providing updates right from the mouth of the Guardian Project team.

In today's inaugural episode, @n8fr8 (Guardian Project Director, Nathan Freitas) provides an update on the Orfox->Tor Browser transition, latest release of Orbot, the new work on orbotmini, Matrix, and a few other exciting new efforts.

[`EnGarde! Episode 1`](https://guardianproject.info/podcast/EnGardePodcast-GuardianProject-Episode1-20190906.aac)

<audio controls="controls" src="https://guardianproject.info/podcast/EnGardePodcast-GuardianProject-Episode1-20190906.aac"/>

